<?php
$form_background = "";
if (isset($_POST["inputBackground"])) {
	$form_background = $_POST["inputBackground"];
}

$form_email = "";
if (isset($_POST["inputEmail"])) {
	$form_email = $_POST["inputEmail"];
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Year's Test - View data page</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body <?php echo (strlen($form_background)>0?" style='background-color: ".$form_background."; color: #ffffff;' ":""); ?>>
    
    <div class="container">
		<h1>New Year's Test - View data page</h1>
      	<h3>Your Email: <?php echo $form_email; ?></h3>
        <h3>Your Background: <?php echo $form_background; ?></h3>
        <a href="index.html" class="btn btn-default">Back to test page</a>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>